@extends('layouts.app')

@section('title')
Связь с нами
@endsection

@section('styles')
  <!-- SPECIFIC CSS -->
  <link href="{{ asset('css/shop.css') }}" rel="stylesheet">

  <!-- Range slider -->
  <link href="{{ asset('css/ion.rangeSlider.css') }}" rel="stylesheet">
  <link href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
@endsection
@section('content')
  <section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('/img/backgrounds/contactus.jpg') }}" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Свяжитесь с нами</h1>
			</div>
		</div>
	</section>
	<!-- End Section -->
  <main>
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="/">Главная</a></li>
					<li>Контакты</li>
				</ul>
			</div>
		</div>
		<!-- End Position -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-md-8 col-sm-8">
					<div class="form_title">
						<h3><strong><i class="icon-pencil"></i></strong>Если у вас есть вопросы, заполните формы</h3>
					</div>
					<div class="step">
						<div id="message-contact"></div>
						<form method="post" action="/contacts/send" id="contactform">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="form-group">
										<label>Ваше имя</label>
										<input type="text" class="form-control" id="name_contact" name="name_contact" placeholder="Введите ваше имя">
									</div>
								</div>
                <div class="col-md-6 col-sm-6">
									<div class="form-group">
										<label>Email</label>
										<input type="email" id="email_contact" name="email_contact" class="form-control" placeholder="Введите email">
									</div>
								</div>
							</div>
							<!-- End row -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Сообщение</label>
										<textarea rows="5" id="message_contact" name="message_contact" class="form-control" placeholder="Сообщение..." style="height:200px;"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label>Human verification</label>

									<input type="submit" value="Отправить" class="btn_1" id="submit-contact">
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- End col-md-8 -->

				<div class="col-md-4 col-sm-4">
					<div class="box_style_1">
						<span class="tape"></span>
						<h4>Адрес <span><i class="icon-pin pull-right"></i></span></h4>
						<p>
							Алматы, Макатаев 37
						</p>
						<hr>
						<h4>Call центр <span><i class="icon-help pull-right"></i></span></h4>
						<ul id="contact-info">
							<li> +7(705)574-82-88 / +7(705)574-82-88</li>
						</ul>
					</div>
					@include('partials.help')
				</div>
				<!-- End col-md-4 -->
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->

		<div id="map_contact"></div>
		<!-- end map-->
		<div id="directions">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<form action="http://maps.google.com/maps" method="get" target="_blank">
							<div class="input-group">
								<input type="text" name="saddr" placeholder="Куда нужно доставить" class="form-control style-2" />
								<input type="hidden" name="daddr" value="Almaty, Almaty 050000" />
								<!-- Write here your end point -->
								<span class="input-group-btn">
					<button class="btn" type="submit" value="Get directions" style="margin-left:0;">Определить</button>
					</span>
							</div>
							<!-- /input-group -->
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- end directions-->
	</main>
	<!-- End main -->
@endsection
@section('scripts')
  <!-- Specific scripts -->
  <script src="{{ asset('assets/validate.js') }}"></script>
  <script src="http://maps.googleapis.com/maps/api/js"></script>
  <script src="{{ asset('js/map_contact.js') }}"></script>
  <script src="{{ asset('js/infobox.js') }}"></script>
@endsection
