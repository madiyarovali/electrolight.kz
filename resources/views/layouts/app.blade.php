<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{ asset('img/apple-touch-icon-57x57-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ asset('img/apple-touch-icon-72x72-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ asset('img/apple-touch-icon-114x114-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ asset('img/apple-touch-icon-144x144-precomposed.png') }}">

    <!-- Google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand%7CLato:300,400%7CMontserrat:400,400i,700,700i" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{ asset('/css/base.css') }}" rel="stylesheet">

    <!-- REVOLUTION SLIDER CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/rev-slider-files/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('/rev-slider-files/fonts/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/rev-slider-files/css/settings.css') }}">
    @yield('styles')
</head>
<body>
    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->


    <!-- Header Plain:  add the id plain to header and change logo.png to logo_sticky.png ======================= -->
    <header id="plain">
      <div id="top_line">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6"><i class="icon-phone"></i><strong>+7 (705)574 82 88</strong>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <ul id="top_links">
                <li>
                  <div class="dropdown dropdown-access">
                    @if (Auth::guest()) <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" id="access_link">Войти / Регистрация</a>@endif
                    <div class="dropdown-menu">
                      @if (Route::has('login'))
                          <div class="top-right links">
                              @auth
                                  <a href="{{ url('/home') }}">Домой</a>
                              @else
                                  <a href="{{ route('login') }}">Войти / </a>
                                  <a href="{{ route('register') }}">Регистрация</a>
                              @endauth
                          </div>
                      @endif
                    </div>
                  </div>
                  <!-- End Dropdown access -->
                </li>
                @if (!Auth::guest())
                  <li><a href="/wishlist" id="wishlist_link">Wishlist</a></li>
                  <li>
                    <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Выйти
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                  </li>
                @endif
              </ul>
            </div>
          </div>
          <!-- End row -->
        </div>
        <!-- End container-->
      </div>
      <!-- End top line-->

      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-3">
            <div id="logo_home">
              <h1><a href="/" title="ЭлектроLight">ЭлектроLight</a></h1>
            </div>
          </div>
          <nav class="col-md-9 col-sm-9 col-xs-9">
                      <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Мобильное меню</span></a>
                      <div class="main-menu">
                          <div id="header_menu">
                              <img src="/img/electrolight.png" width="160" height="34" alt="ЭлектроLight" data-retina="true">
                          </div>
                          <a href="javascript:void(0);" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                          <ul>
                              <li class="megamenu submenu">
                                  <a href="javascript:void(0);" class="show-submenu-mega">Категории <i class="icon-down-open-mini"></i></a>
                                  <div class="menu-wrapper">
                                      <form role="search" method="get" action="/search">
                                        <div class="form-group" style="padding: 5px">
                                          <input type="text" name="q" placeholder="Поиск..." class="form-control">
                                        </div>
                                      </form>
                                      @foreach (\App\Category::children()->chunk(10) as $chunk)
                                      <div class="col-md-4">
                                        <ul>
                                          @foreach($chunk as $child)
                                          <li><a href="/category/{{ $child->id }}" style="font-size: 10px;">
                                            @if(strlen($child->title) <= 40)
                                              {{ $child->title }}
                                            @elseif(strlen($child->title) > 40)
                                              <abbr title="{{$child->title}}">{{ substr($child->title, 0) }}...</abbr>
                                            @endif
                                          </a></li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endforeach
                                  </div>
                              </li>
                              @if (Auth::guest())
                              <li class="submenu">
                                  <a href="/login" class="show-submenu">Войдите </i></a>
                              </li>
                              @else
                              <li class="submenu">
                                  <a href="/home" class="show-submenu">Домой </i></a>
                              </li>
                              <li class="submenu">
                                  <a href="/history" class="show-submenu">История заказов </i></a>
                              </li>
                              <li><a href="/wishlist" id="wishlist_link">Избранное</a></li>
                              <li>
                                <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            Выйти
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                              </li>
                              @endif
                              <li class="submenu">
                                  <a href="/contacts" class="show-submenu">Задать вопрос / Контакты </a>
                              </li>
                          </ul>
                      </div><!-- End main-menu -->
                      <ul id="top_tools">
                          <li>
                              <div class="dropdown dropdown-search">
                                  <a href="header_2.html#" class="search-overlay-menu-btn" data-toggle="dropdown"><i class="icon-search"></i></a>
                              </div>
                          </li>
                          <li>
                              <div class="dropdown dropdown-cart">
                                    @if (!(Auth::guest()))
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class=" icon-basket-1"></i>Корзина </a>
                                      <ul class="dropdown-menu" id="cart_items">
                                        @foreach(\App\Order::cart() as $item)
                                          <li>
                                              <div class="image">
                                                  <img src="/{!! $item->item->image !!}" alt="image">
                                              </div>
                                              <strong>
                                                  <a href="/item/{{ $item->item->id }}">{{ $item->item->title }}</a>{{ $item->qty }} x {{ $item->item->price }}
                                              </strong>
                                              <form action="/order/remove" class="form2" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $item->id }}">
                              								  <button type="submit"><i class="icon-trash"></i></button>
                              								</form>
                                          </li>
                                        @endforeach
                                      </ul>
                                    @endif
                                </div>
                              <!-- End dropdown-cart-->
                          </li>
                      </ul>
                  </nav>
        </div>
      </div>
      <!-- container -->
    </header>
    <div style="z-index: 1000000">
      @yield('content')
    </div>
        
    <footer id="footer_3" class="revealed" style="z-index: -1000000 !important;">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-4 col-sm-3">
  					<p><img src="/img/electrolight_white.png" width="160" height="34" alt="City tours" data-retina="true" id="logo">
  					</p>
  					<p>О нас. Мы рады вас видеть в нашем интернет магазине Electrolight. Занимаемся продажей электрических приборов, ламп, прожекторов и т.д.(полный список в разделе категории). Сотрудничаем с такими компаниями как ТОО «Электрокомплект-1», MEGALIGHT, IEK, DKC, «Navigator», Schneider Electric, Osram, КЭАЗ и т.д. . Чтобы заказать у нас товар Вам необходимо зарегистрироваться. Данные покупателей никогда не передаем третьим лицам. На рынке с ... года. Перешли в онлайн 10.12.2017. <br> Наш официальный сайт: electrolight.kz </p>
  				</div>
  				<div class="col-md-3 col-sm-3">
  					<!-- <ul>
  						<li><a href="footer_4.html#">Войти</a>
  						</li>
  						<li><a href="footer_4.html#">Регистрация</a>
  						</li>
  					</ul> -->
  				</div>
  				</div>
  			<!-- End row -->
  			<!-- End row -->
  		</div>
  		<!-- End container -->
  	</footer>
  	<!-- End footer -->
    <div class="col-md-2" style="position: fixed;bottom: 50px;z-index: 100;right: 50px;">
      <div class="box_style_2" id="message" style="display:none;">

      </div>
    </div>

    <div id="toTop"></div><!-- Back to top button -->

    <!-- Search Menu -->
    <div class="search-overlay-menu">
      <span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
      <form role="search" id="searchform" method="get" action="/search">
        <input value="" name="q" type="search" placeholder="Поиск..." />
        <button type="submit"><i class="icon_set_1_icon-78"></i>
        </button>
      </form>
    </div>
    <!-- End Search Menu -->
    <!-- Common scripts -->
    <script src="{{ asset('/js/jquery-2.2.4.min.js') }}"></script>
  	<script src="{{ asset('/js/common_scripts_min.js') }}"></script>
    <script src="{{ asset('/js/functions.js') }}"></script>
    <script src="{{ asset('/js/ajax2.js') }}"></script>
    <script src="{{ asset('/js/fade.js') }}"></script>
  	<!-- <script src="{{ asset('/js/ajax.js') }}"></script> -->

    @yield('scripts')

</body>
</html>
