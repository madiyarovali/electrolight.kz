@extends('layouts.app')

@section('title')
Главная | Electrolight.kz
@endsection

@section('styles')
  <!-- REVOLUTION SLIDER CSS -->
	<link href="{{ asset('/layerslider/css/layerslider.css') }}" rel="stylesheet">
@endsection

@section('content')
<main>
  <!-- Slider -->
  <div id="full-slider-wrapper">
    <div id="layerslider" style="width:100%;height:600px;">
      <!-- first slide -->
      <!-- <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:5;">
        <img src="img/slides/slide_1.jpg" class="ls-bg" alt="Slide background">
        <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Более 6000 товаров на данный момент</h3>
        <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Ламповая продукция / Щитовое оборудование / Низковольтная аппаратура</p>
        <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='/category/1'>Узнать побольше</a>
      </div> -->

      <!-- second slide -->
      <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:85;">
        <img src="img/slides/slide_2.jpg" class="ls-bg" alt="Slide background">
        <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Регулярное обновление товаров</h3>
        <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Последнее обновление было: {{ $lastUpdate->created_at->format('Y.M.d') }}</p>
        <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='/newest'>Узнать побольше</a>
      </div>

      <!-- third slide -->
      <div class="ls-slide" data-ls="slidedelay:5000; transition2d:103;">
        <img src="img/slides/slide_3.jpg" class="ls-bg" alt="Slide background">
        <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Возможность доставки</h3>
        <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">При заказе более 50 000тг. доставка бесплатная</p>
        <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='/pages/info/1'>Узнать побольше</a>
      </div>

      <!-- fourth slide -->
      <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:14;">
        <img src="img/slides/slide_4.jpg" class="ls-bg" alt="Slide background">
        <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Легкая оплата</h3>
        <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Возможность оплатить наличными, на счет</p>
        <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='/pages/info/2'>Узнать побольше</a>
      </div>

    </div>
  </div>
  <!-- End layerslider -->

  <div class="container margin_60">

    <div class="main_title">
      <h2>Интернет магазин <span>электротехники</span></h2>
      <p>г. Алматы 2017г.</p>
    </div>

    <div class="row">
			@foreach($items as $item)
      <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.{!! $loop->index + 1 !!}s">
        <div class="tour_container">
          <div class="ribbon_3 popular"><span>Популярные</span>
          </div>
          <div class="img_container">
            <a href="/item/{{ $item->id }}">
              <img src="/{!! $item->image !!}" class="img-responsive" alt="Image">
              <div class="short_info">
                <i class="icon_set_1_icon-44"></i>{{ $item->category->title }}<span class="price">{{ $item->price }}<sup>тг.</sup></span>
              </div>
            </a>
          </div>
          <div class="tour_title">
            <h3><strong>{{ $item->title }}</strong></h3>
            <!-- end rating -->
						@if(!(Auth::guest()))
						<div class="wishlist">
							<form action="/wishlist/addOrRemove" method="post">
								{{ csrf_field() }}
								<input type="hidden" value="{{ $item->id }}" name="item_id">
								@if(in_array($item->id, json_decode(App\Wishlist::wishlist())))
									<i class="btns icon-heart" style="transition: 0.2s;"></i>
								@else
									<i class="btns icon-heart-empty" style="transition: 0.2s;"></i>
								@endif
							</form>
						</div>
						@endif
            <!-- End wish list-->
          </div>
        </div>
        <!-- End box tour -->
      </div>
      <!-- End col-md-4 -->
 			@endforeach

    </div>
    <!-- End row -->
    <p class="text-center add_bottom_30">
      <a href="/newest" class="btn_1 medium"><i class="icon-eye-7"></i>Просмотреть все недавно добавленные</a>
    </p>

    <hr>

  </div>
  <!-- End container -->

  <div class="white_bg">
    <div class="container margin_60">
      <div class="main_title">
        <h2><span>Категории</span></h2>
        <p>
          Ниже в таблице приведен каталог
        </p>
      </div>
      <div class="row add_bottom_45">
				@foreach(\App\Category::children() as $category)
        <div class="col-md-4 other_tours">
          <ul>
            <li><a href="/category/{{ $category->id }}"><i class="icon_set_1_icon-3"></i>{{ $category->title }}<span class="other_tours_price"></span></a></li>
          </ul>
        </div>
				@endforeach
      </div>
      <!-- End row -->

    <!-- End container -->
  </div>
  <!-- End white_bg -->


  <div class="container margin_60">

    <div class="main_title">
      <h2>Несколько<span> выгодных </span>сторон в сотрудничестве с Нами</h2>
    </div>

    <div class="row">

      <div class="col-md-4 wow zoomIn" data-wow-delay="0.2s">
        <div class="feature_home">
          <i class="icon_set_1_icon-41"></i>
          <h3><span>+6000</span> только качественных продукции</h3>
        </div>
      </div>

      <div class="col-md-4 wow zoomIn" data-wow-delay="0.4s">
        <div class="feature_home">
          <i class="icon_set_1_icon-30"></i>
          <h3><span>Разумные </span>цены на продукцию и доставку</h3>
        </div>
      </div>

      <div class="col-md-4 wow zoomIn" data-wow-delay="0.6s">
        <div class="feature_home">
          <i class="icon_set_1_icon-57"></i>
          <h3><span>Поддержка</span></h3>
        </div>
      </div>

    </div>
    <!--End row -->

    <hr>

    <div class="row">
      <div class="col-md-8 col-sm-6 hidden-xs">
        <img src="img/laptop.png" alt="Laptop" class="img-responsive laptop">
      </div>
      <div class="col-md-4 col-sm-6">
        <h3>Начните <span>прямо сейчас</span></h3>
        <ul class="list_order">
          <li><span>1</span>Выберите то что Вам нужно</li>
          <li><span>2</span>Закиньте в корзину</li>
          <li><span>3</span>Подтвердите свой заказ, мы доставим на следующий день</li>
        </ul>
        <a href="/newest" class="btn_1">Начать сейчас</a>
      </div>
    </div>
    <!-- End row -->

  </div>
  <!-- End container -->
</main>
<!-- End main -->

@endsection
@section('scripts')
<!-- Specific scripts -->
	<script src="{{ asset('/layerslider/js/greensock.js') }}"></script>
	<script src="{{ asset('/layerslider/js/layerslider.transitions.js') }}"></script>
	<script src="{{ asset('/layerslider/js/layerslider.kreaturamedia.jquery.js') }}"></script>
	<script src="{{ asset('/js/addOrRemove.js') }}" charset="utf-8"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			'use strict';
			$('#layerslider').layerSlider({
				autoStart: true,
				responsive: true,
				responsiveUnder: 1280,
				layersContainer: 1170,
				skinsPath: 'layerslider/skins/'
					// Please make sure that you didn't forget to add a comma to the line endings
					// except the last line!
			});
		});
	</script>
@endsection
