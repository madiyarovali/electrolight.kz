@extends('layouts.app')

@section('title')
Результаты поиска по запросу {{ $query }}
@endsection

@section('styles')
  <!-- SPECIFIC CSS -->
  <link href="{{ asset('css/shop.css') }}" rel="stylesheet">

  <!-- Range slider -->
  <link href="{{ asset('css/ion.rangeSlider.css') }}" rel="stylesheet">
  <link href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
@endsection
@section('content')

	<section class="parallax-window" data-parallax="scroll" data-image-src="/img/backgrounds/result.jpg" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Результаты поиска</h1>
			</div>
		</div>
	</section>
	<!-- End Section -->

	<main>
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="/">Главная</a></li>
					<li>Результаты по поиску {{ $query }}</li>
				</ul>
			</div>
		</div>
		<!-- End Position -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-md-12">
					<div class="shop-section">

						<div class="items-sorting">
							<div class="row">
								<div class="col-xs-6">
									<div class="results_shop">
									   Найдены {{ count($results) }} результата
									</div>
								</div>
								<div class="col-xs-6 col-md-12">
                    <form action="/search" method="get">
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <input type="hidden" name="q" value="{{ $request->q }}">
    										<select name="sort_by">
    											<option selected disabled>Сортировать...</option>
    											<option value="lower">По дате добавления</option>
    											<option value="higher">По цене</option>
    										</select>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-6">
                        <input type="submit" value="Отсортировать" class="btn_1">
                      </div>
                    </form>
								</div>
							</div>
						</div>
						<!--End Sort By-->

						<div class="row">
              @foreach($results as $result)
  							<div class="shop-item col-lg-4 col-md-6 col-sm-6">
  								<div class="inner-box">
  									<div class="image-box"  style="height: 450px;">
  										<figure class="image">
  											<a href="/item/{{ $result->id }}">
                          <img src="/{!! $result->image !!}" alt="{{ $result->title }}" style="height: 350px;">
  											</a>
  										</figure>
  										<div class="item-options clearfix">
                        @if(!(Auth::guest()))
                        <form action="/order/add" method="post" class="form1">
                          {{ csrf_field() }}
                          <input type="hidden" name="status" value="1">
                          <button type="submit" class="btn_shop btn">
                            <span class="icon-basket"></span>
                            <div class="tool-tip">
                                Добавить в корзину
                            </div>
                          </button>
        								</form>
                        @endif
                        @if(!(Auth::guest()))
                        <div class="wishlist"  style="top:-31px;right:2px;">
          								<form action="/wishlist/addOrRemove" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ $result->id }}" name="item_id">
                            @if(in_array($result->id, json_decode(App\Wishlist::wishlist())))
                              <i class="btns icon-heart" style="transition: 0.2s;"></i>
                            @else
                              <i class="btns icon-heart-empty" style="transition: 0.2s;"></i>
                            @endif
                          </form>
                        </div>
          							@endif
                        <a href="/item/{{ $result->id }}" class="btn_shop">
                          <span class="icon-eye"></span>
                          <div class="tool-tip">
                             Просмотр
                          </div>
                        </a>
  										</div>
  									</div>
  									<div class="product_description"  style="padding: 0px 20px;">
  										<h3><a href="/item/{{ $result->id }}">{{ $result->title }}</a></h3>
  										<div class="price">
                        @if($result->is_spec != 0)
                          Цена: <span class="offer">{{ $result->price }} тг.</span> {{ $result->price * ((100 - $result->is_spec) / 100) }} тг.
                        @else
                          Цена: <span class="offer"></span> {{ $result->price }} тг.
                        @endif
  										</div>
  									</div>
  								</div>
  							</div>
  							<!--End Shop Item-->
              @endforeach
						</div>
						<!--End Shop Item-->

						<hr>

						<div class="text-center">
							{{ $results->appends(['q' => $request->q, 'sort_by' => $request->sort_by])->links()}}
						</div>
						<!-- End pagination-->
					</div>
					<!-- End row -->
				</div>
				<!-- End col -->
			</div>
		</div>
		<!-- End Container -->
	</main>
	<!-- End main -->
@endsection

@section('scripts')
<script src="{{ asset('/js/addOrRemove.js') }}" charset="utf-8"></script>
<script>
  if ($('.prod-tabs .tab-btn').length) {
    $('.prod-tabs .tab-btn').on('click', function (e) {
      e.preventDefault();
      var target = $($(this).attr('href'));
      $('.prod-tabs .tab-btn').removeClass('active-btn');
      $(this).addClass('active-btn');
      $('.prod-tabs .tab').fadeOut(0);
      $('.prod-tabs .tab').removeClass('active-tab');
      $(target).fadeIn(500);
      $(target).addClass('active-tab');
    });

  }
</script>
@endsection
