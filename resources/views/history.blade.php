
@extends('layouts.app')

@section('title')
Ваша история покупок
@endsection

@section('styles')
  <!-- SPECIFIC CSS -->
  <link href="{{ asset('css/shop.css') }}" rel="stylesheet">

  <!-- Range slider -->
  <link href="{{ asset('css/ion.rangeSlider.css') }}" rel="stylesheet">
  <link href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
@endsection
@section('content')

	<section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('/img/backgrounds/home.png') }}" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Ваша История</h1>
				<small>(на этой странице можете отслеживать статус заказа)</small>
			</div>
		</div>
	</section>
	<!-- End Section -->

	<main>
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="/">Главная</a>
					</li>
					<li>История</li>
				</ul>
			</div>
		</div>
		<!-- End Position -->

		<div class="container margin_60">
			<div class="cart-section">
				<table class="table table-striped cart-list shopping-cart">
					<thead>
            <a href="{{ URL::previous() }}" class="btn medium">Назад</a>
						<tr>
							<th>
								Продукт
							</th>
							<th>
								Статус
							</th>
							<th>
								Количество
							</th>
							<th>
                Начальная цена
							</th>
              <th>
								Скидка
							</th>
							<th>
								Сумма
							</th>
						</tr>
					</thead>
					<tbody>
            @foreach($orders as $order)
						<tr>
							<td>
								<div class="thumb_cart">
									<a href="/item/{{ $order->item->id }}"><img src="/{!! $order->item->image !!}" alt="{{ $order->item->title }}">
									</a>
								</div>
								<strong class="item_cart">{{ $order->item->title }}</strong>
							</td>
							<td>
								<strong>
                  @if($order->status == 1)
                    Ожидайте подтверждение заказа
                  @elseif($order->status == 2)
                    Ваш заказ принят
                  @elseif($order->status == 3)
                    Ваш заказ отменен
                  @elseif($order->status == 4)
                    Ваш заказ отклонен
                  @elseif($order->status == 5)
                    Ваш заказ успешно выслан
                  @endif
								</strong>
							</td>
							<td>
								{{ $order->qty }}
							</td>
							<td>
								<strong>{{ $order->item->price }} Тг.</strong>
							</td>
              <td>
                <strong>{{ $order->item->is_spec }} %</strong>
							</td>
							<td>
                <strong>{{ $order->item->price * ((100 - $order->item->is_spec) / 100) * $order->qty }} Тг.</strong>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
          {{ $orders->links() }}
			</div>
		</div>
		<!-- End Container -->
	</main>
	<!-- End main -->

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/ajax1.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/ajax2.js') }}"></script>
<script>
		if ($('.prod-tabs .tab-btn').length) {
			$('.prod-tabs .tab-btn').on('click', function (e) {
				e.preventDefault();
				var target = $($(this).attr('href'));
				$('.prod-tabs .tab-btn').removeClass('active-btn');
				$(this).addClass('active-btn');
				$('.prod-tabs .tab').fadeOut(0);
				$('.prod-tabs .tab').removeClass('active-tab');
				$(target).fadeIn(500);
				$(target).addClass('active-tab');
			});

		}
	</script>
@endsection
