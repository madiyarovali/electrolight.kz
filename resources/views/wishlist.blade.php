@extends('layouts.app')

@section('title')
"Избранное"
@endsection

@section('styles')
  <!-- CSS -->
  <link href="css/base.css" rel="stylesheet">

  <!-- Radio and check inputs -->
  <link href="css/skins/square/grey.css" rel="stylesheet">
@endsection
@section('content')
<section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('/img/backgrounds/wishlist.jpg') }}" data-natural-width="1400" data-natural-height="470">
  <div class="parallax-content-1">
    <div class="animated fadeInDown" style="background: rgba(0, 0, 0, 0.5)">
      <h1>"Избранное"</h1>
      <p>Здесь хранятся Ваши товары помеченные как "Избранное"</p>
    </div>
  </div>
</section>
<!-- End section -->

<main>
  <div id="position">
    <div class="container">
      <ul>
        <li><a href="/">Главная</a></li>
        <li>"Избранное"</li>
      </ul>
    </div>
  </div>
  <!-- Position -->

  <div class="container margin_60">

    <div class="row">
      <aside class="col-lg-4 col-md-4">
        <div class="box_style_2">
          Имя: {{ Auth::user()->name }} <br>
          Телефон: {{ Auth::user()->number }} <br>
          Адрес: {{ Auth::user()->address }}
        </div>
        @include('partials.help')
      </aside>
      <!--End aside -->
      <div class="col-lg-8 col-md-8">

        <div id="tools">

          <div class="row">
            <form action="/wishlist" method="get">
              <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="styled-select-filters">
                  <select name="sort_price" id="sort_price">
                    <option value="" selected disabled>Сортировка по цене</option>
                    <option value="lower">Сначала дешевые</option>
                    <option value="higher">Сначала дорогие</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="styled-select-filters">
                  <select name="sort_rating" id="sort_rating">
                    <option value="" selected disabled>Сначала ...</option>
                    <option value="lower">Новые</option>
                    <option value="higher">Популярные</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="">
                    <input type="submit" value="Подобрать" class="btn">
                </div>
              </div>
            </form>

          </div>
        </div>
        <!--/tools -->
        @foreach($items as $item)
        <div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
              <div class="wishlist_close">-</div>
              <div class="img_list">
                <a href="/item/{{ $item->id }}">
                  <div class="ribbon popular"></div><img src="/{!! $item->image !!}" alt="Image">
                  <div class="short_info"><i class="icon_set_1_icon-4"></i>{{ $item->category->title }} </div>
                </a>
              </div>
            </div>
            <div class="clearfix visible-xs-block"></div>
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="tour_list_desc">
                <h3><strong>{{ $item->title }}</strong></h3>
                <p>{{ substr($item->body, 0, 100) }}...</p>
                <ul class="add_info">
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-83"></i></span>
                      <div class="tooltip-content">
                        <h4>Код продукта: </h4>
                        <strong>{{ $item->code_item }}</strong>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                      <div class="tooltip-content">
                        <h4>Артикул продукта: </h4>
                        <strong>{{ $item->artic }}</strong>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-97"></i></span>
                      <div class="tooltip-content">
                        <h4>Гарантия продукта: </h4>
                        <strong>{{ $item->guarant }}</strong>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-27"></i></span>
                      <div class="tooltip-content">
                        <h4>Скидка на данный продукт:  </h4>
                        <strong>{{ $item->is_spec }}%</strong>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="tooltip_styled tooltip-effect-4">
                      <span class="tooltip-item"><i class="icon_set_1_icon-25"></i></span>
                      <div class="tooltip-content">
                        <h4>Наличие:
                          @if($item->is_able == 1)
                            <strong>В наличии</strong>
                          @else
                            <strong>Нет в наличии</strong>
                          @endif
                        </h4>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
              <div class="price_list">
                <div>
                  @if($item->is_spec != 0)
                    {{ $item->price * ((100 - $item->is_spec) / 100) }}<sup>тг.</sup><span class="normal_price_list">{{ $item->price }}тг.</span>
                  @else
                    {{ $item->price }}тг.<br>
                  @endif
                  <br>
                  <p>
                    <a href="/item/{{ $item->id }}" class="btn_1">Детали</a>
                  </p>
                </div>

              </div>
            </div>
          </div>
        </div>
        <!--End strip -->
        @endforeach
        <hr>

        <div class="text-center">
          {{ $items->appends(['sort_price' => $request->sort_price, 'sort_rating' => $request->sort_rating])->links() }}
        </div>
        <!-- end pagination-->

      </div>
      <!-- End col lg-9 -->
    </div>
    <!-- End row -->
  </div>
  <!-- End container -->
</main>
<!-- End main -->
@endsection

@section('scripts')
<!-- Specific scripts -->
	<!-- Cat nav mobile -->
	<script src="js/cat_nav_mobile.js"></script>
	<script>
		$('#cat_nav').mobileMenu();
	</script>
	<!-- Check and radio inputs -->
	<script src="js/icheck.js"></script>
  <script src="{{ asset('/js/addOrRemove.js') }}" charset="utf-8"></script>

	<script>
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-grey',
			radioClass: 'iradio_square-grey'
		});
	</script>
	<!-- Map -->
	<script src="http://maps.googleapis.com/maps/api/js"></script>
	<script src="js/map.js"></script>
	<script src="js/infobox.js"></script>
	<script>
		$('.wishlist_close').on('click', function (c) {
			$(this).parent().parent().fadeOut('slow', function (c) {});
		});
	</script>
@endsection
