@extends('layouts.app')

@section('title')
Подтверждение
@endsection

@section('styles')
  <!-- CSS -->
  <link href="{{ asset('css/base.css') }}" rel="stylesheet">

  <!-- Radio and check inputs -->
  <link href="{{ asset('css/skins/square/grey.css') }}" rel="stylesheet">
@endsection
@section('content')
  <section id="hero_2">
		<div class="intro_title animated fadeInDown">
			<h1>Разместить заказ</h1>
			<div class="bs-wizard">

				<div class="col-xs-4 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum">Ваша корзина</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="/cart" class="bs-wizard-dot"></a>
				</div>

				<div class="col-xs-4 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum">Ваши данные</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="/payment" class="bs-wizard-dot"></a>
				</div>

				<div class="col-xs-4 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum">Подтверждение</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="/confirmation" class="bs-wizard-dot"></a>
				</div>

			</div>
			<!-- End bs-wizard -->
		</div>
		<!-- End intro-title -->
	</section>
	<!-- End Section hero_2 -->
	<main>
    <div id="position">
			<div class="container">
				<ul>
					<li><a href="/">Главная</a></li>
          <li><a href="/cart">Корзина</a></li>
          <li><a href="/payment">Данные</a></li>
					<li>Подтверждение</li>
				</ul>
			</div>
		</div>

		<div class="container margin_60">
			<div class="row">
				<div class="col-md-8 add_bottom_15">

					<div class="form_title">
						<h3><strong><i class="icon-ok"></i></strong>Спасибо за покупку!</h3>
						<p>
              Еще немного и мы отправим вам ваш заказ
						</p>
					</div>
					<div class="step">
						<p>
							Точка отправки: {{ $user->address }} <br>
              Ваш номер телефона: {{ $user->number }}
						</p>
					</div>
					<!--End step -->

					<div class="form_title">
						<h3><strong><i class="icon-tag-1"></i></strong>Товары</h3>
					</div>
					<div class="step">
            @foreach($orders as $order)
						<table class="table confirm">
							<thead>
								<tr>
									<th colspan="2">
										Номер товара: {{ $loop->index + 1 }}
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<strong>{{ $order->item->title }}</strong>
									</td>
									<td>
										{{ $order->qty }} шт.
									</td>
								</tr>
								<tr>
									<td>
										<strong></strong>
									</td>
									<td>
	                   {{ $order->created_at->format('Y.M.d') }}
									</td>
								</tr>
								<tr>
									<td>
										<strong>Кому: </strong>
									</td>
									<td>
										{{ $user->name }}
									</td>
								</tr>
							</tbody>
						</table>
            @endforeach
					</div>
					<!--End step -->
				</div>
				<!--End col-md-8 -->

				<aside class="col-md-4">
					<div class="box_style_1">
						<h3 class="inner">Спасибо за покупку!</h3>
						<p>
							Ниже вы можете увидить свой чек
						</p>
						<hr>
						<a class="btn_full_outline" href="/invoice" target="_blank">Чек</a>
					</div>
					@include('partials.help')
				</aside>

			</div>
			<!--End row -->
		</div>
		<!--End container -->
	</main>
	<!-- End main -->
@endsection

@section('scripts')
  <script src="js/icheck.js"></script>
	<script>
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-grey',
			radioClass: 'iradio_square-grey'
		});
	</script>
@endsection
