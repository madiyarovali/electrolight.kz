@extends('layouts.app')
@section('title')
Регистрация
@endsection

@section('styles')
    <!-- CSS -->
    <link href="/css/flickity.css" rel="stylesheet">
@endsection

@section('content')
<main>
    <section id="hero" class="login">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                	<div id="login">
                    		<div class="text-center"><img src="img/electrolight.png" alt="Image" data-retina="true" class="img-responsive" ></div>
                            <hr>
                            <form method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                	<label>Как Вас звать</label>
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                	<label>Номер телефона</label>
                                    <input id="number" type="text" class="form-control" name="number" value="{{ old('number') }}" required autofocus>

                                    @if ($errors->has('number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                	<label>Адрес</label>
                                    <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus>

                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                	<label>Email</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                	<label>Пароль</label>
                                    <input id="password1" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                	  <label>Подтвердите Ваш пароль</label>
                                    <input id="password2" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                                <div id="pass-info" class="clearfix"></div>
                                <button type="submit" class="btn btn-primary">
                                    Регистрация
                                </button>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </section>
	</main>
  <!-- End main -->
@endsection
