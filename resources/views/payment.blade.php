@extends('layouts.app')

@section('title')
Ваши данные
@endsection

@section('styles')
  <!-- CSS -->
  <link href="{{ asset('css/base.css') }}" rel="stylesheet">

  <!-- Radio and check inputs -->
  <link href="{{ asset('css/skins/square/grey.css') }}" rel="stylesheet">
@endsection
@section('content')

  <section id="hero_2" style="background: url('/img/backgrounds/payment.jpg') no-repeat center center; ">
    <div class="intro_title animated fadeInDown">
      <h1>Разместить заказ</h1>
      <div class="bs-wizard">

        <div class="col-xs-4 bs-wizard-step complete">
          <div class="text-center bs-wizard-stepnum">Ваша корзина</div>
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="/cart" class="bs-wizard-dot"></a>
        </div>

        <div class="col-xs-4 bs-wizard-step active">
          <div class="text-center bs-wizard-stepnum">Ваши данные</div>
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="/payment" class="bs-wizard-dot"></a>
        </div>

        <div class="col-xs-4 bs-wizard-step disabled">
          <div class="text-center bs-wizard-stepnum">Подтверждение</div>
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="/confirmation" class="bs-wizard-dot"></a>
        </div>

      </div>
      <!-- End bs-wizard -->
    </div>
    <!-- End intro-title -->
  </section>

	<main>
    <div id="position">
			<div class="container">
				<ul>
					<li><a href="/">Главная</a></li>
          <li><a href="/cart">Корзина</a></li>
          <li>Данные</li>
				</ul>
			</div>
		</div>
		<!-- End position -->

    <div class="container margin_60">
			<div class="row">
				<div class="col-md-8 add_bottom_15">
					<div class="form_title">
						<h3><strong>1</strong>Ваши данные</h3>
						<strong>Эти данные никогда не передаются третьим лицам.</strong>
					</div>
          <form action="/checkout" method="post">
            {{ csrf_field() }}
            <div class="step">
  						<div class="row">
  							<div class="col-md-6 col-sm-6">
  								<div class="form-group">
  									<label>Ваше имя</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
  								</div>
  							</div>
                <div class="col-md-6 col-sm-6">
  								<div class="form-group">
  									<label>Телефон</label>
  									<input type="tel" id="number" name="number" class="form-control" value="{{ $user->number }}">
  								</div>
  							</div>
  						</div>
  						<div class="row">
  							<div class="col-md-6 col-sm-6">
  								<div class="form-group">
  									<label>Адрес</label>
  									<input type="text" id="address" name="address" class="form-control" value="{{ $user->address }}">
  								</div>
  							</div>
                <div class="col-md-6 col-sm-6">
  								<div class="form-group">
  									<label>Заметки</label>
  									<input type="text" id="tips" name="tips" class="form-control">
  								</div>
  							</div>
  						</div>
  					</div>
  					<!--End step -->
            <button type="submit" class="btn_1 green medium" name="button">Подтвердить</button>
          </form>
				</div>


			</div>
			<!--End row -->
		</div>
		<!--End container -->
	</main>
	<!-- End main -->
@endsection

@section('scripts')
  <!-- Specific scripts -->
  <script src="js/icheck.js"></script>
  <script>
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-grey',
      radioClass: 'iradio_square-grey'
    });
  </script>
@endsection
