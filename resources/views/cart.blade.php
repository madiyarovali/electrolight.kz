@extends('layouts.app')

@section('title')
Ваша корзина
@endsection

@section('styles')
  <!-- CSS -->
	<link href="{{ asset('css/jquery.switch.css') }}" rel="stylesheet">
@endsection

@section('content')
<section id="hero_2">
		<div class="intro_title animated fadeInDown">
			<h1>Разместить заказ</h1>
			<div class="bs-wizard">

				<div class="col-xs-4 bs-wizard-step active">
					<div class="text-center bs-wizard-stepnum">Ваша корзина</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="/" class="bs-wizard-dot"></a>
				</div>

				<div class="col-xs-4 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Ваши данные</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="/payment" class="bs-wizard-dot"></a>
				</div>

				<div class="col-xs-4 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Подтверждение</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="/confirmation" class="bs-wizard-dot"></a>
				</div>

			</div>
			<!-- End bs-wizard -->
		</div>
		<!-- End intro-title -->
	</section>
	<!-- End Section hero_2 -->
<main>
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="/">Главная</a></li>
					<li>Корзина</li>
				</ul>
			</div>
		</div>
		<!-- End position -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-md-8">
					<table class="table table-striped cart-list add_bottom_30">
						<thead>
							<tr>
								<th>
									Имя товара
								</th>
								<th>
									Количество
								</th>
								<th>
									Скидка
								</th>
								<th>
									Общее
								</th>
								<th>
									Действия
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($orders as $order)
							@if($order->status == 0)
							<tr>
								<td>
									<div class="thumb_cart">
										<img src="/{!! $order->item->image !!}" alt="Image">
									</div>
									<span class="item_cart">{{ $order->item->title }}</span>
								</td>
								<td>
										<form action="/order/update" method="post" class="form1">
			                  {{ csrf_field() }}
			                  <input type="hidden" name="order_id" value="{{ $order->id }}">
			                  <input type="hidden" name="status" value="1">
												<input type="number" min="1" value="{{ $order->qty }}" class="qty2 form-control" name="qty">
										</form>
								</td>
								<td>
									{{ $order->item->is_spec }}%
								</td>
								<td>
									<strong>{{ $order->qty * $order->item->price * ((100 - $order->item->is_spec) / 100) }} тг.</strong>
								</td>
								<td class="options">
									<form action="/order/remove" class="form2" method="post">
	                  {{ csrf_field() }}
	                  <input type="hidden" name="id" value="{{ $order->id }}">
									  <button type="submit"><i class="icon-trash"></i></button>
									</form>
								</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>
					<table class="table table-striped options_cart">
						<thead>
							<tr>
								<th colspan="3">
									Дополнительные услуги / Сервисы
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width:10%">
									<i class="icon_set_1_icon-16"></i>
								</td>
								<td style="width:60%">
									Доставка <strong>+500 тг/км</strong>
								</td>
								<td style="width:35%">
									<label class="switch-light switch-ios pull-right">
										<input type="checkbox" name="option_1" id="option_1" checked value="">
										<span>
                    <span>Нет</span>
										<span>Да</span>
										</span>
										<a></a>
									</label>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- End col-md-8 -->

				<aside class="col-md-4">
					<div class="box_style_1">
						<h3 class="inner">- Итого -</h3>
						<table class="table table_summary">
							<tbody>
								@foreach($orders as $order)
								@if($order->status == 1)
								<tr>
									<td>
										{{ $order->item->title }}
									</td>
									<td class="text-right">
										{{ $order->item->price * (( 100 - $order->item->is_spec) / 100) * $order->qty }}
									</td>
								</tr>
								@endif
								@endforeach
								<tr class="total">
									<td>Общая стоимость</td>
									<td class="text-right">

									</td>
								</tr>
							</tbody>
						</table>
						<form action="/payment" method="post"><button type="submit" class="btn_full">Перейти к оплате</button>{{ csrf_field() }}</form>
						<a class="btn_full_outline" href="{{ URL::previous() }}"><i class="icon-right"></i> Продолжить покупку</a>
					</div>
					@include('partials.help')
				</aside>
				<!-- End aside -->

			</div>
			<!--End row -->
		</div>
		<!--End container -->
	</main>
	<!-- End main -->

@endsection
@section('scripts')
<!-- Specific scripts -->
	<script src="{{ asset('layerslider/js/greensock.js') }}"></script>
	<script src="{{ asset('layerslider/js/layerslider.transitions.js') }}"></script>
	<script src="{{ asset('layerslider/js/layerslider.kreaturamedia.jquery.js') }}"></script>
	<script src="{{ asset('/js/ajax1.js') }}"></script>
	<script src="{{ asset('/js/ajax2.js') }}"></script>
	<script type="text/javascript">
		var val = 0;
		$(".text-right").each(function(index) {
				val += +$(this).text();
		});
		$('tr.total > .text-right').text(val);
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			'use strict';
			$('#layerslider').layerSlider({
				autoStart: true,
				responsive: true,
				responsiveUnder: 1280,
				layersContainer: 1170,
				skinsPath: 'layerslider/skins/'
					// Please make sure that you didn't forget to add a comma to the line endings
					// except the last line!
			});
		});
	</script>
@endsection
