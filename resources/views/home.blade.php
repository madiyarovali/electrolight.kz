@extends('layouts.app')

@section('title')
Ваша домашняя страница
@endsection

@section('styles')
  <!-- SPECIFIC CSS -->
  <link href="{{ asset('css/shop.css') }}" rel="stylesheet">

  <!-- Range slider -->
  <link href="{{ asset('css/ion.rangeSlider.css') }}" rel="stylesheet">
  <link href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
@endsection
@section('content')

	<section class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('/img/backgrounds/home.png') }}" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Ваша корзина</h1>
			</div>
		</div>
	</section>
	<!-- End Section -->

	<main>
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="/">Главная</a>
					</li>
					<li>Профиль</li>
				</ul>
			</div>
		</div>
		<!-- End Position -->

		<div class="container margin_60">
			<div class="cart-section">
				<table class="table table-striped cart-list shopping-cart">
					<thead>
						<tr>
							<th>
								Продукт
							</th>
							<th>
								Цена
							</th>
							<th>
								Количество
							</th>
							<th>
								Сумма
							</th>
							<th>
								Удалить
							</th>
						</tr>
					</thead>
					<tbody>
            @foreach($orders as $order)
						<tr>
							<td>
								<div class="thumb_cart">
									<a href="/item/{{ $order->item->id }}"><img src="/{!! $order->item->image !!}" alt="{{ $order->item->title }}">
									</a>
								</div>
								<strong class="item_cart">{{ $order->item->title }}</strong>
							</td>
							<td>
								<strong>{{ $order->item->price }}</strong>
							</td>
							<td>
								<form action="/order/update" method="post" class="form1">
                  {{ csrf_field() }}
                  <input type="hidden" name="order_id" value="{{ $order->id }}">
                  <input type="hidden" name="status" value="1">
									<input type="number" min="1" value="{{ $order->qty }}" class="qty2 form-control" name="qty">
								</form>
							</td>
							<td>
								<strong>{{ $order->item->price * ((100 - $order->item->is_spec) / 100) * $order->qty }} Тг.</strong>
							</td>
							<td class="options">
								<form action="/order/remove" class="form2" method="post">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" value="{{ $order->id }}">
								  <button type="submit"><i class="icon-trash"></i></button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
        {{ $orders->links() }}
			</div>
		</div>
		<!-- End Container -->
	</main>
	<!-- End main -->

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/ajax1.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/ajax2.js') }}"></script>
<script>
		if ($('.prod-tabs .tab-btn').length) {
			$('.prod-tabs .tab-btn').on('click', function (e) {
				e.preventDefault();
				var target = $($(this).attr('href'));
				$('.prod-tabs .tab-btn').removeClass('active-btn');
				$(this).addClass('active-btn');
				$('.prod-tabs .tab').fadeOut(0);
				$('.prod-tabs .tab').removeClass('active-tab');
				$(target).fadeIn(500);
				$(target).addClass('active-tab');
			});

		}
	</script>
@endsection
