@extends('layouts.app')

@section('title')
Детали продукта {{ $item->title }}
@endsection

@section('styles')
  <!-- SPECIFIC CSS -->
  <link href="{{ asset('css/shop.css') }}" rel="stylesheet">

  <!-- Range slider -->
  <link href="{{ asset('css/ion.rangeSlider.css') }}" rel="stylesheet">
  <link href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
@endsection
@section('content')
<section class="parallax-window" data-parallax="scroll" data-image-src="/img/backgrounds/details.jpg" data-natural-width="1400" data-natural-height="470">
  <div class="parallax-content-1">
    <div class="animated fadeInDown" style="background: rgba(0, 0, 0, 0.5)">
      <h1>{{ $item->title }}</h1>
      <p>Детали продукта</p>
    </div>
  </div>
</section>
<!-- End Section -->

<main>
  <div id="position">
    <div class="container">
      <ul>
        <li><a href="/">Главная</a></li>
        <li><a href="/category/{{ $item->category_id }}">{{ $item->category->title }}</a></li>
        <li>{{ $item->title }}</li>
      </ul>
    </div>
  </div>
  <!-- End Position -->

  <div class="container margin_60">
    <div class="row">
      <div class="col-md-9">

        <div class="product-details">

          <div class="basic-details">
            <div class="row">
              <div class="image-column col-md-6 col-sm-6 col-xs-12">
                <figure class="image-box">
                    <img src="/{!! $item->image !!}" alt="">
                </figure>
              </div>
              <div class="info-column col-md-6 col-sm-6 col-xs-12">
                <div class="details-header">
                  <div><h2>{{ $item->title }}</h2></div>
                  <div class="item-price">
                    @if($item->is_spec != 0)
                      Цена: <span class="offer">{{ $item->price }} тг.</span> {{ $item->price * ((100 - $item->is_spec) / 100) }} тг.
                    @else
                      Цена: <span class="offer"></span> {{ $item->price }} тг.
                    @endif
                  </div>
                  {{ $item->views }} <span class="icon-eye"></span>
                </div>
                <div class="text">
                  <h4>Код продукта: {{ $item->code_item }}</h4>
                  <h4>Артикул продукта: {{ $item->artic }}</h4>
                  <h4>Гарантия продукта: {{ $item->guarant }}</h4>
                  @if($item->is_spec != NULL)
                    <h4>Скидка на данный продукт:  {{ $item->is_spec }} %</h4>
                  @endif
                  <h4>Наличие:
                    @if($item->is_able == 1)
                      В наличии
                    @else
                      Нет в наличии
                    @endif
                  </h4>
                  <h4>Дата добавления: {{ $item->updated_at->format('Y-M-d') }} </h4>
                </div>
                <div class="other-options">
                  <div class="row">
                    <form action="/order/add" method="post" class="form1 form-inline">
                      {{ csrf_field() }}
                      <input type="hidden" name="status" value="1">
                      <input type="hidden" name="item_id" value="{{ $item->id }}">
                      <div class="col-md-3">
    									  <input type="number" min="1" value="1" class="qty2 form-control" name="qty">
                      </div>
                      <div class="col-md-4">
                        <input type="submit" value="Добавить в корзину" class="btn_1 medium">
                      </div>
    								</form>
                  </div>
                </div>
                <!--Item Meta-->
                <ul class="item-meta">
                  <li>Категория: <a href="/category/{{ $item->category->id }}">{{ $item->category->title }}</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!--End Basic Details-->

          <div class="product-info-tabs">

            <div class="prod-tabs" id="product-tabs">
              <div class="tab-btns clearfix">
                <a href="shop-single.html#prod-description" class="tab-btn active-btn">Описание</a>
              </div>

              <div class="tabs-container">
                <div class="tab active-tab" id="prod-description">
                  <h3>Описание</h3>
                  <div class="content">
                    <p>
                      {!! $item->body !!}
                        @if(!(Auth::guest()))
                        <div class="wishlist">
          								<form action="/wishlist/addOrRemove" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ $item->id }}" name="item_id">
                            @if(in_array($item->id, json_decode(App\Wishlist::wishlist())))
                              <i class="btns icon-heart" style="transition: 0.2s;"></i>
                            @else
                              <i class="btns icon-heart-empty" style="transition: 0.2s;"></i>
                            @endif
                          </form>
                        </div>
          							@endif
                    </p>
                  </div>
                </div>
                <!--End Tab-->

              </div>
              <!--End tabs-container-->
            </div>
            <!--End prod-tabs-->
          </div>
          <!--End product-info-tabs-->

          <div class="related-products">
            <div class="normal-title">
              <h3>Похожие продукты</h3>
            </div>
            <div class="row">
              @foreach($relatedItems as $item)
              <div class="shop-item col-lg-4 col-md-6 col-sm-6">
                <div class="inner-box">
                  <div class="image-box">
                    <figure class="image">
                      <a href="/item/{{ $item->id }}"><img src="/{!! $item->image !!}" alt="">
                      </a>
                    </figure>
                    <div class="item-options clearfix">
                      @if(!(Auth::guest()))
                        <form action="/order/add" method="post" class="form1">
                          {{ csrf_field() }}
                          <input type="hidden" name="status" value="1">
                          <button type="submit" class="btn_shop btn">
                            <span class="icon-basket"></span>
                            <div class="tool-tip">
                                Добавить в корзину
                            </div>
                          </button>
        								</form>
                        <div class="wishlist" style="top:-31px;right:2px;">
          								<form action="/wishlist/addOrRemove" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ $item->id }}" name="item_id">
                            @if(in_array($item->id, json_decode(App\Wishlist::wishlist())))
                              <i class="btns icon-heart" style="transition: 0.2s;"></i>
                            @else
                              <i class="btns icon-heart-empty" style="transition: 0.2s;"></i>
                            @endif
                          </form>
                        </div>
        							@endif
                      <a href="/item/{{ $item->id }}" class="btn_shop">
                        <span class="icon-eye"></span>
                        <div class="tool-tip">
                           Просмотр
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="product_description">
                    <div class="rating">

                    </div>
                    <h3><a href="/item/{{ $item->id }}">{{ $item->title }}</a></h3>
                    <div class="price">
                      <span class="offer"></span> {{ $item->price }} тг.
                    </div>
                  </div>
                </div>
              </div>
              <!--End Shop Item-->
              @endforeach
            </div>
          </div>
          <!--End Related products-->
        </div>
        <!--End Product-details-->
      </div>
      <!--End Col-->

      <div class="col-md-3">
        <aside class="sidebar">
          <div class="widget">
              <form action="/search" method="get">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Поиск..." name="q">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" style="margin-left:0;"><i class="icon-search"></i></button>
                  </span>
                </div>
              </form>
          </div>
          <!-- End Search -->
          <hr>
          <div class="widget" id="cat_shop">
            <h4>Категории</h4>
            <ul>
              @foreach($categories as $category)
                <li><a href="/category/{{ $category->id }}">{{ $category->title }}</a></li>
              @endforeach
            </ul>
          </div>
          <!-- End widget -->
          <hr>
        </aside>
      </div>
      <!--Sidebar-->
    </div>
  </div>
  <!-- End Container -->
</main>
<!-- End main -->
@endsection

@section('scripts')
<script src="{{ asset('/js/addToCart.js') }}" charset="utf-8"></script>
<script src="{{ asset('/js/addOrRemove.js') }}" charset="utf-8"></script>
<script>
  if ($('.prod-tabs .tab-btn').length) {
    $('.prod-tabs .tab-btn').on('click', function (e) {
      e.preventDefault();
      var target = $($(this).attr('href'));
      $('.prod-tabs .tab-btn').removeClass('active-btn');
      $(this).addClass('active-btn');
      $('.prod-tabs .tab').fadeOut(0);
      $('.prod-tabs .tab').removeClass('active-tab');
      $(target).fadeIn(500);
      $(target).addClass('active-tab');
    });

  }
</script>
@endsection
