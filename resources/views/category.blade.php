@extends('layouts.app')

@section('title')
ElectroLight | {{ $category->title }}
@endsection

@section('styles')
  <!-- Radio and check inputs -->
  <link href="{{ asset('css/skins/square/grey.css') }}" rel="stylesheet">

  <!-- Range slider -->
  <link href="{{ asset('css/ion.rangeSlider.css') }}" rel="stylesheet">
  <link href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
@endsection
@section('content')
  <section class="parallax-window" data-parallax="scroll" data-image-src="/img/backgrounds/category.jpg" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>{{ $category->title }}</h1>
			</div>
		</div>
	</section>
	<!-- End section -->
  <main>
    <div id="position">
      <div class="container">
        <ul>
          <li><a href="/">Главная</a></li>
          <li>{{ $category->title }}</li>
        </ul>
      </div>
    </div>
    <!-- Position -->

    <div class="container margin_60">
      <div class="row">
        <aside class="col-lg-3 col-md-3">

          <div id="filters_col">
            <a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt"><i class="icon_set_1_icon-65"></i>Фильтры <i class="icon-plus-1 pull-right"></i></a>
            <div class="collapse in" id="collapseFilters">
              <form action="/result" method="get">
                <div class="filter_type">
                  <h6>Цена</h6>
                  <input type="text" id="range" name="range" value="">
                </div>
                @foreach($filters as $filter)
                <div class="filter_type">
                  <h6>{{ $filter->parameterName }}</h6>
                  <ul>
                    @foreach(json_decode($filter->parameters) as $params)
                    <li>
                      <label>
                          <input type="checkbox">
                          <span class="filter">
                            {{ $params }}
                          </span>
                        </label>
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endforeach
                <input type="submit" value="Подобрать" class="btn">
              </form>
            </div>
            <!--End collapse -->
          </div>
          <!--End filters col-->
          @include('partials.help')
        </aside>
        <!--End aside -->

        <div class="col-lg-9 col-md-8">
          <div id="tools">
            <div class="row">
              <form action="/category/{{ $category->id }}" method="get">
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="styled-select-filters">
                    <select name="sort_price" id="sort_price">
                      <option value="" selected disabled>Сортировка по цене</option>
                      <option value="lower">Сначала дешевые</option>
                      <option value="higher">Сначала дорогие</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="styled-select-filters">
                    <select name="sort_rating" id="sort_rating">
                      <option value="" selected disabled>Сначала ...</option>
                      <option value="lower">Новые</option>
                      <option value="higher">Популярные</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="">
                      <input type="submit" value="Подобрать" class="btn">
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!--End tools -->
          <div class="row" id="update">
          @foreach($items as $item)
            <div class="col-md-6 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
              <div class="hotel_container">
                @if($item->view > 100)
                <div class="ribbon_3 popular">
                  <span>Популярное</span>
                </div>
                @endif
                <div class="img_container">
                  <a href="/item/{{ $item->id }}">
                    <img src="/{!! $item->image !!}" width="800" height="533" class="img-responsive" alt="Image">
                    <div class="short_info hotel">
                      <span class="price">{{ $item->price }}<sup>тг.</sup></span>
                    </div>
                  </a>
                </div>
                <div class="hotel_title">
                  <h3><strong>{{ $item->title }}</strong></h3>
                  <!-- end rating -->
      							@if(!(Auth::guest()))
                    <div class="wishlist">
      								<form action="/wishlist/addOrRemove" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $item->id }}" name="item_id">
                        @if(in_array($item->id, json_decode(App\Wishlist::wishlist())))
                          <i class="btns icon-heart" style="transition: 0.2s;"></i>
                        @else
                          <i class="btns icon-heart-empty" style="transition: 0.2s;"></i>
                        @endif
                      </form>
                    </div>
      							@endif
                  <!-- End wish list-->
                </div>
              </div>
              <!-- End box tour -->
            </div>
            <!-- End col-md-6 -->
            @endforeach
          </div>
          <hr>
          <div class="row">
            <div class="text-center">
              {{ $items->appends(['sort_price' => $request->sort_price, 'sort_rating' => $request->sort_rating])->links() }}
            </div>
            <!-- end pagination-->
          </div>


        </div>
        <!-- End col lg 9 -->
      </div>
      <!-- End row -->
    </div>
    <!-- End container -->
  </main>
  <!-- End main -->
@endsection


@section('scripts')
<script src="{{ asset('/js/addOrRemove.js') }}" charset="utf-8"></script>
@endsection
