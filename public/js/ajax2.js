$('.form2').on('submit', function(e) {
		// Stop the browser from submitting the form.
		e.preventDefault();
		var form = $(this);
		// Get the messages div.
		var formMessages = $('.alert');
		// Serialize the form data.
		var formData = $(form).serialize();

		// Submit the form using AJAX.
		$.ajax({
			type: 'POST',
			url: $(form).attr('action'),
			data: formData
		})
		.done(function(response) {
			// Make sure that the formMessages div has the 'success' class.
			$(formMessages).removeClass('error');
			$(formMessages).addClass('success');

			// Set the message text.
			$('#message').append(response);
			
		})
		.fail(function(data) {
			// Make sure that the formMessages div has the 'error' class.
			$(formMessages).removeClass('success');
			$(formMessages).addClass('error');

			// Set the message text.
			if (data.responseText !== '') {
				$('#message').append(data.responseText);
			} else {
				$('#message').append('Oops! An error occured and your message could not be sent.');
			}
		});
		$(form.parent().parent()).fadeOut("slow", function(){
			//
		});
	});
