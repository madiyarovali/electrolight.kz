<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


# HomeController----------------------------------------------------------------
Route::get('/home', 'HomeController@index');
Route::get('/history', 'HomeController@history');
#-------------------------------------------------------------------------------


# MailController---------------------------------------------------------------
Route::get('/contacts', 'MailController@contacts');

Route::post('/contacts/send', 'MailController@mailSend');
#-------------------------------------------------------------------------------

# CategoryController------------------------------------------------------------
Route::get('/category/{id}', 'CategoryController@index');
#-------------------------------------------------------------------------------

# WishlistController------------------------------------------------------------
Route::get('/wishlist', 'WishlistController@index');

Route::post('/wishlist/addOrRemove', 'WishlistController@addOrRemove');
#-------------------------------------------------------------------------------

# SearchController--------------------------------------------------------------
Route::get('/search', 'SearchController@index');

# Работает
#-------------------------------------------------------------------------------

# ItemController----------------------------------------------------------------
Route::get('/', 'ItemController@index');
Route::get('/newest', 'ItemController@newest');
Route::get('/item/{id}', 'ItemController@details');

// Route::get('/excel', 'ItemController@getData');
#-------------------------------------------------------------------------------

# OrderController---------------------------------------------------------------
Route::get('/cart', 'OrderController@index');
Route::post('/payment', 'OrderController@payment');

Route::post('/checkout', 'OrderController@checkout');
Route::post('/order/add', 'OrderController@addToCart');
Route::post('/order/update', 'OrderController@updateCart');
Route::post('/order/remove', 'OrderController@deleteFromCart');
#-------------------------------------------------------------------------------


# PdfController-----------------------------------------------------------------
Route::get('/invoice', 'PdfController@index');
#-------------------------------------------------------------------------------


# ParsingController-------------------------------------------------------------
Route::get('/parsing/items', 'ParsingController@items');
#-------------------------------------------------------------------------------
