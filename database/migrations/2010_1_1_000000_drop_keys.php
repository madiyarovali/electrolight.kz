<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('views', function(Blueprint $table) {
            $table->dropForeign(['id']);
        });
        Schema::dropIfExists('views');

        Schema::table('orders', function(Blueprint $table) {
            $table->dropForeign(['user_id', 'item_id']);
        });
        Schema::dropIfExists('orders');

        Schema::table('items', function(Blueprint $table) {
            $table->dropForeign(['category_id']);
        });

        Schema::table('images', function(Blueprint $table) {
            $table->dropForeign(['item_id']);
        });
        Schema::dropIfExists('images');

        Schema::table('filters', function(Blueprint $table) {
            $table->dropForeign(['category_id']);
        });
        Schema::dropIfExists('filters');
    }
}
