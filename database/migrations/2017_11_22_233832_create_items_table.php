<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_item');
            $table->string('artic')->nullable();
            $table->string('guarant')->nullable();
            $table->string('title');
            $table->text('body')->nullable();
            $table->integer('price');
            $table->integer('views')->nullable();
            $table->boolean('is_able');
            $table->integer('is_spec')->nullable();
            $table->integer('category_id')->unsigned();
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
