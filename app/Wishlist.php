<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = [
        'user_id',
        'item_id',
        'comments'
    ];

    public static function wishlist()
    {
        $wishlist = json_encode(Wishlist::whereIn('user_id', [Auth::id()])->pluck('item_id'));
        return $wishlist;
    }


    public function item()
    {
        return $this->belongsTo('App\Item');
    }
}
