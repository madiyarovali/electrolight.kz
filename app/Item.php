<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'code_item',
        'artic',
        'guarant',
        'title',
        'body',
        'price',
        'views',
        'is_able',
        'image',
        'category_id',
        'is_spec'
    ];


    // belongsTo
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    // hasMany
    public function image()
    {
        return $this->hasMany('App\Image');
    }
    public function order()
    {
        return $this->hasMany('App\Order');
    }
    public function views()
    {
        return $this->hasMany('App\View');
    }
}
