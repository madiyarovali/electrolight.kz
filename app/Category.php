<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'parent',
        'image',
        'title',
        'comment'
    ];

    public static function parents()
    {
        $categories = Category::whereIn('parent', [1])->get();
        return $categories;
    }

    public static function children()
    {
        $categories = Category::where('parent', '>', 1)->get();
        return $categories;
    }


    // hasMany
    public function item()
    {
        return $this->hasMany('App\Item');
    }
    public function view()
    {
        return $this->hasMany('App\View');
    }
    public function filter()
    {
        return $this->hasMany('App\Filter');
    }
}
