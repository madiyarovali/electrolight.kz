<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
      protected $fillable = [
          'item_id',
          'path'
      ];

      // belongsTo
      public function item()
      {
          return $this->belongsTo('App\Item');
      }
      public function UserProfile()
      {
          return $this->belongsTo('App\UserProfile');
      }
}
