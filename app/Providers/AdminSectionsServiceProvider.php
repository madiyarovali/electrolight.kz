<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
      \App\Admin::class => 'App\Http\Sections\Admin',
      \App\Category::class => 'App\Http\Sections\Category',
      \App\Filter::class => 'App\Http\Sections\Filter',
      \App\Image::class => 'App\Http\Sections\Image',
      \App\Item::class => 'App\Http\Sections\Item',
      \App\Mail::class => 'App\Http\Sections\Mail',
      \App\Order::class => 'App\Http\Sections\Order',
      \App\Page::class => 'App\Http\Sections\Page',
      \App\User::class => 'App\Http\Sections\Users',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
