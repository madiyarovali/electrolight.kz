<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable = [
      'dollar'
    ];

    public static function dollarToTenge()
    {
        $dollar = Admin::orderBy('created_at', 'DESC')->first();
        return $dollar->dollar;
    }
}
