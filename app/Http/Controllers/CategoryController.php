<?php

namespace App\Http\Controllers;

use App\Category;
use App\Filter;
use App\Item;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(Request $request) // category view
    {
        try {
            $category = Category::find($request->id);
            $filters = Filter::where('category_id', $request->id)->get();

            $items = Item::where('category_id', $request->id)->paginate(20);
            // ^-- Default Query
            if($request->sort_price == 'lower')$items = Item::where('category_id', $request->id)->orderBy('price', 'ASC')->paginate(20);
            // ^-- Сортировка по цене(Возр)
            else if($request->sort_price == 'higher')$items = Item::where('category_id', $request->id)->orderBy('price', 'DESC')->paginate(20);
            // ^-- Сортировка по цене(Пониж)

            if($request->sort_rating == 'lower')$items = Item::where('category_id', $request->id)->orderBy('created_at', 'DESC')->paginate(20);
            // ^-- Сортировка по дате
            else if($request->sort_rating == 'higher')$items = Item::where('category_id', $request->id)->orderBy('views', 'DESC')->paginate(20);
            // ^-- Сортировка по просмотрам

            return view('category', compact('items', 'category', 'request', 'filters'));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function categories()
    {
        try {
            $categories = Category::whereIn('parent', [1])->paginate(10);
            
            return view('categories', compact('categories'));
        } catch (Exception $e) {
            return $e;
        }
    }
}
