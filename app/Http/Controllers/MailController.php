<?php

namespace App\Http\Controllers;

use App\Mail;
use App\UserProfile;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
# GET---------------------------------------------------------------------------

    public function contacts()
    {
        try {
            return view('contact');
        } catch (Exception $e) {
            return $e;
        }

    }

# POST--------------------------------------------------------------------------

    public function mailSend(Request $request)
    {
        try {
            $mail = new Mail;
            $mail->email = $request->email;
            $mail->name = $request->name;
            $mail->body = $request->message;
            $mail->save();
            return "Ваше сообщение доставлено удачно, ожидайте ответ.";
        } catch (Exception $e) {
            return $e;
        }

    }
}
