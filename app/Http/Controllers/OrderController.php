<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;

use Mail;
use Auth;
// use App\Http\Middleware\isValid;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//0 => 'В корзине', 1 => 'В рассмотрении', 2 => 'Заказ принят', 3 => 'Отменен', 4 => 'Отказано', 5 => 'Успешно выслан']

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware(isValid::class);
    }


// GET -------------------------------------------------------------------------

    public function index() // cart view
    {
        try {
            $orders = Order::where('user_id', Auth::id())->whereIn('status', [0])->orderBy('created_at', 'DESC')->paginate(20);
            $exists = Order::where('user_id', Auth::id())->where('status', 0)->value('id');
            if(!($exists >= 1)) {
                return redirect('/home');
            }
            return view('cart', compact('orders'));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function payment(Request $request)
    {
        try {
            $user = User::find(Auth::id());
            // ^-- Thats not so good expression

            return view('payment', compact('user'));
        } catch (Exception $e) {
            return $e;
        }

    }

    public function checkout(Request $request)
    {
        try {
            $orders = Order::where('user_id', Auth::id())->whereIn('status', [0])->orderBy('created_at', 'DESC')->paginate(20);
            // ^-- Only that orders which are in cart
            $user = User::find(Auth::id());
            // ^-- Thats not so good expression

            foreach($orders as $order){
                $order->update(['status' => 1]);
                if($order->status != 1) {
                    $msg = "Somethings went wrong <br>Что-то пошло не так";
                }
            }

            $msg = "Successfully accepted. Wait for a call from the manager <br>Успешно принято, ожидайте звонок от менеджера";

            return view('checkout', compact('orders', 'user', 'msg'));
        } catch (Exception $e) {
            return $e;
        }
    }

// POST ------------------------------------------------------------------------

    public function addToCart(Request $request)
    {
        try {
            $order = Order::find($request->order_id);
            if($order != null){
                updateCart($request);
            }else if($order == null) {
                $newOrder = new Order;
                $newOrder->item_id = $request->item_id;
                $newOrder->user_id = Auth::id();
                $newOrder->status = 0;
                $newOrder->qty = $request->qty;
                $newOrder->save();
            }

            return "Successfully added <br>Добавлено удачно";
        } catch (Exception $e) {
            return $e;
        }

    }


    public function updateCart(Request $request)
    {
        try {
          $item = Order::where('id', $request->order_id)->update([
              'qty' => $request->qty,
              'status' => 0
          ]);
          return "Successfully updated <br>Обновлено удачно";
        } catch (Exception $e) {
          return $e;
        }

    }


    public function deleteFromCart(Request $request)
    {
        try {
            $item = Order::where('id', $request->id)->delete();
            return "Successfully removed <br>Удалено удачно";
        } catch (Exception $e) {
            return $e;
        }

    }



}
