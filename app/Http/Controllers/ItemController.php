<?php

namespace App\Http\Controllers;

use App\Category;
use App\Image;
use App\Item;
use App\Wishlist;

use Excel;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{

# GET---------------------------------------------------------------------------
    public function index() // welcome view
    {
        try {
            $items = Item::orderBy('created_at', 'DESC')->take(6)->get();

            $categories = Category::whereIn('parent', [1])->get();
            $childCategories = Category::where('parent', '>', [1])->get();

            $lastUpdate = Item::orderBy('created_at', 'DESC')->first();

            return view('welcome', compact('items', 'categories', 'childCategories', 'lastUpdate'));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function newest()
    {
        $items = Item::orderBy('created_at', 'DESC')->paginate(20);
        return view('newest', compact('items'));
    }

    public function details(Request $request)
    {
        try {
            $item = Item::find($request->id);

            $relatedItems = Item::where('category_id', $item->category_id)->orderBy('views', 'DESC')->take(10)->get();

            $categories = Category::whereIn('parent', [1])->get();

            $images = Image::where('item_id', $request->id)->get();
            return view('productDetails', compact('item', 'categories', 'images', 'relatedItems'));
        } catch (Exception $e) {
            return $e;
        }
    }

# GetFromExcel -----------------------------------------------------------------
    // public function getData()
    // {
    //   $prices = [
    //     2550,
    //     7095,
    //     1350,
    //     2310,
    //     2380,
    //     3060,
    //     3650,
    //     5650,
    //     8610,
    //     11280,
    //     12820,
    //     15190,
    //     19060,
    //     22650,
    //     22990,
    //     27190,
    //     9980,
    //     13280,
    //     18480,
    //     1630,
    //     1850,
    //     1850,
    //     1850,
    //     1880,
    //     2355,
    //     3260,
    //     6199,
    //     1880,
    //     1990,
    //     2780,
    //     4680,
    //     3180,
    //     3590,
    //     1980,
    //     2150,
    //     2530,
    //     1680,
    //     1820,
    //     2320,
    //     620,
    //     750,
    //     1070,
    //     1450,
    //     1450,
    //     395,
    //     620,
    //     750,
    //     750,
    //     1030,
    //     1030,
    //     1145,
    //     1580,
    //     1060,
    //     1480,
    //     3820,
    //     2560,
    //     3690,
    //     6910,
    //     5390,
    //     5390,
    //     2900,
    //     630,
    //     1690,
    //     1180,
    //     1035,
    //     1370,
    //     1970,
    //     2310,
    //     2060,
    //     625,
    //     1650,
    //     2120,
    //     2280,
    //     1890,
    //     1080,
    //     1220,
    //     1770,
    //     1890,
    //     3350,
    //     2150,
    //     2315,
    //     3630,
    //     4830,
    //     1690,
    //     2150,
    //     2190,
    //     2390,
    //     3850,
    //     7060,
    //     2060,
    //     2615,
    //     2610,
    //     3040,
    //     3540,
    //     3540,
    //     3540,
    //     25250,
    //     29840,
    //     6450,
    //     2750,
    //     2750,
    //     4465,
    //     24880,
    //     27430,
    //     6450,
    //     4780,
    //     5370,
    //     5510,
    //     2890,
    //     3990,
    //     4260,
    //     6060,
    //     265,
    //     270,
    //     265,
    //     315,
    //     265,
    //     265,
    //     265,
    //     265,
    //     315,
    //     345,
    //     340,
    //     345,
    //     345,
    //     345,
    //     395,
    //     485,
    //     735,
    //     1295,
    //     1420,
    //     1830,
    //     6650,
    //     480,
    //     585,
    //     585,
    //     205,
    //     346,
    //     346,
    //     358,
    //     280,
    //     325,
    //     299,
    //     328,
    //     328,
    //     328,
    //     328,
    //     399,
    //     520,
    //     360,
    //     390,
    //     410,
    //     410,
    //     410,
    //     410,
    //     445,
    //     445,
    //     438,
    //     438,
    //     438,
    //     360,
    //     455,
    //     455,
    //     455,
    //     455,
    //     495,
    //     495,
    //     495,
    //     495,
    //     495,
    //     495,
    //     545,
    //     545,
    //     545,
    //     555,
    //     555,
    //     555,
    //     635,
    //     740,
    //     635,
    //     635,
    //     1440,
    //     1490,
    //     1490,
    //     2130,
    //     2130,
    //     2470,
    //     2490,
    //     2840,
    //     2840,
    //     158,
    //     150,
    //     170,
    //     309,
    //     205,
    //     990,
    //     1090,
    //     1190,
    //     1280,
    //     1565,
    //     1767,
    //     515,
    //     847,
    //     1258,
    //     136,
    //     186,
    //     236,
    //     375,
    //     650,
    //     730,
    //     700,
    //     1055,
    //     790,
    //     1210,
    //     1310];
    //   $i = 0;
    //   Excel::selectSheets('Заказ')->load('load.xls', function($reader) use ($i, $prices){
    //       $reader->each(function($row) use ($i, $prices){
    //           $row->dump();
    //           $item = new Item;
    //           $item->title = $row->title;
    //           $item->code_item = $row->code_item;
    //           $item->price = $prices[$i];
    //           $item->image = '/images/uploads/'.$row->category_id.'.png';
    //           $item->category_id = $row->category_id;
    //           $item->is_able = 1;
    //           $item->save();
    //       });
    //   });
    //
    //
    // }


}
