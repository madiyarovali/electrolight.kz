<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        try {
            $query = $request->q;
            $results = Item::where('body', 'LIKE', '%'.$query.'%')->orWhere('title', 'LIKE', '%'.$query.'%')->paginate(20);
            // ^-- Search

            if($request->sort_by == 'lower')$results = Item::where('body', 'LIKE', '%'.$query.'%')->orWhere('title', 'LIKE', '%'.$query.'%')->orderBy('created_at', 'DESC')->paginate(20);
            // ^-- Сортировка по цене(Возр)
            else if($request->sort_by == 'higher')$results = Item::where('body', 'LIKE', '%'.$query.'%')->orWhere('title', 'LIKE', '%'.$query.'%')->orderBy('price', 'DESC')->paginate(20);
            // ^-- Сортировка по цене(Пониж)

            return view('results', compact('results', 'request', 'query'));
        } catch (Exception $e) {
            return $e;
        }
    }
}
