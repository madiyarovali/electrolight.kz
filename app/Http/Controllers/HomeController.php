<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;

use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

     // GET--------------------------------------------------------------------------
     public function index() // home view
     {
         try {
             $user = User::find(Auth::id());
             $orders = Order::where('user_id', Auth::id())->whereIn('status', [0])->paginate(20);

             return view('home', compact('user', 'orders'));
         } catch (Exception $e) {
             return $e;
         }
     }

     public function history()
     {
         try {
             $orders = Order::where('user_id', Auth::id())->where('status', '>', 0)->orderBy('created_at', 'DESC')->paginate(10);
             return view('history', compact('orders'));
         } catch (Exception $e) {
             return $e;
         }
     }



 // POST-------------------------------------------------------------------------
     public function updateData(Request $request)
     {
         try {
             User::find(Auth::id())->update([
                 'address' => $request->address,
                 'number' => $request->number,
                 'name' => $request->name
             ]);
             return redirect('/home');
         } catch (Exception $e) {
             return $e;
         }

     }
}
