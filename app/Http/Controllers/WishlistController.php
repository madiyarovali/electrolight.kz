<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;
use App\Wishlist;

use Auth;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

# GET---------------------------------------------------------------------------
  public function index(Request $request)
  {
      try {
          $ids = Wishlist::whereIn('user_id', [Auth::id()])->pluck('item_id');
          $items = Item::whereIn('id', $ids)->paginate(20);

          if($request->sort_price == "lower") {
              $items = Item::whereIn('id', $ids)->orderBy('price', 'ASC')->paginate(20);
          }else if($request->sort_price == "higher"){
              $items = Item::whereIn('id', $ids)->orderBy('price', 'DESC')->paginate(20);
          }
          if ($request->sort_rating == "lower") {
              $items = Item::whereIn('id', $ids)->orderBy('created_at', 'DESC')->paginate(20);
          }else if($request->sort_rating == "higher"){
              $items = Item::whereIn('id', $ids)->orderBy('views', 'DESC')->paginate(20);
          }

          return view('wishlist', compact('request', 'items'));
      } catch (Exception $e) {
          return $e;
      }
  }
#-------------------------------------------------------------------------------


# POST--------------------------------------------------------------------------
  public function addOrRemove(Request $request)
  {
      try {
          $item = Wishlist::where('user_id', Auth::id())->where('item_id', $request->item_id)->value('id');

          if($item == null){
              $items = new Wishlist;
              $items->item_id = $request->item_id;
              $items->user_id = Auth::id();
              $items->save();
              return "This item successfully added to wishlist <br>Этот товар удачно добавлен в 'Избранные'";
          }
          else if($item != null){
              Wishlist::where('user_id', Auth::id())->where('item_id', $request->item_id)->delete();
              return "This item successfully removed from wishlist <br>Этот товар удачно удален из 'Избранных'";
          }
      } catch (Exception $e) {
          return $e;
      }
  }

#-------------------------------------------------------------------------------

}
