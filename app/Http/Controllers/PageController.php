<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;
use App\Order;
use App\Status;
use App\Wishlist;

use Auth;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

# GET---------------------------------------------------------------------------

    // public function success(Request $request)
    // {
    //     try {
    //         return view('success');
    //     } catch (Exception $e) {
    //         return redirect('404')->with($e);
    //     }
    //
    // }

# POST--------------------------------------------------------------------------

    public function filter(Request $request)
    {
        try {
            $price = filter_var($request->price_range, FILTER_SANITIZE_NUMBER_INT);
            $price1 = stristr($price, '-', true);
            $price2 = stristr($price, '-');
            $price2 = substr($price2, 1);
            if (is_array($request->category)) $request->category = implode($request->category);
            $categories = explode(",", $request->category);

            // remove all empty values from array
            $array = array_filter($categories, function($value) { return $value !== ''; });
            // ----------------------------------

            $sortBy = 'created_at';
            $method = 'DESC';
            if ($request->sortBy == 'newer') {
                $sortBy = 'created_at';
                $method = 'DESC';
            }
            else if($request->sortBy == 'popular') {
                $sortBy = 'views';
                $method = 'DESC';
            }
            else if($request->sortBy == 'up') {
                $sortBy = 'price';
                $method = 'ASC';
            }
            else if($request->sortBy == 'down') {
                $sortBy = "'price', 'DESC'";
                $method = 'DESC';
            }

            $items = Item::whereIn('category_id', $categories)->where('price', '>', $price1 / \App\Admin::dollarToTenge())->where('price', '<', $price2 / \App\Admin::dollarToTenge())->orderBy($sortBy, $method)->paginate(20);

            return view('results', compact('items', 'request'));
        } catch (Exception $e) {
            return redirect('404')->with($e);
        }
    }


    public function filterCategory(Request $request)
    {
          try {
              $price = filter_var($request->price_range, FILTER_SANITIZE_NUMBER_INT);
              $price1 = stristr($price, '-', true);
              $price2 = stristr($price, '-');
              $price2 = substr($price2, 1);

              if (is_array($request->properties)) $request->properties = implode($request->properties);
              $properties = explode(",", $request->properties);

              // remove all empty values from array
              $array = array_filter($properties, function($value) { return $value !== ''; });
              // ----------------------------------


              $sortBy = 'created_at';
              $method = 'DESC';
              if($request->sortBy == 'popular') {
                  $sortBy = 'views';
              }
              else if($request->sortBy == 'up') {
                  $sortBy = 'price';
                  $method = 'ASC';
              }
              else if($request->sortBy == 'down') {
                  $sortBy = "'price', 'DESC'";
                  $method = 'DESC';
              }

              $items = Item::where('price', '>', $price1 / \App\Admin::dollarToTenge())->where('price', '<', $price2 / \App\Admin::dollarToTenge())->orderBy($sortBy, $method)->paginate(20);
              // dd($request->properties);
              return view('results', compact('items', 'request'));
          } catch (Exception $e) {
              return redirect('404')->with($e);
          }
    }
}
