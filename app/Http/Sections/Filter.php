<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

/**
 * Class Filter
 *
 * @property \App\Filter $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Filter extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
     public function initialize()
     {
        $this->addToNavigation($priority = 500, function() {
            return \App\Filter::count();
        });
     }
     public function getTitle() {
        return 'Фильтры';
     }
     public function getIcon()
     {
        return 'fa fa-picture-o';
     }
    public function onDisplay()
    {
        return AdminDisplay::table()
                 ->setHtmlAttribute('class', 'table-primary')
                 ->setColumns(
                     AdminColumn::text('id', '#')->setWidth('30px'),
                     AdminColumn::link('category.title', 'Категория' )->setWidth('100px'),
                     AdminColumn::link('parameterName', 'Параметр' )->setWidth('100px')
                 )
                 ->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
      return AdminForm::panel()->addBody([
          AdminFormElement::text('parameterName', 'Наименование параметра')->required(),
          AdminFormElement::select('category_id', 'Категория', \App\Category::pluck('title', 'id')->all())->required(),
          AdminFormElement::text('parameters', 'Параметры(через пробел)'),
      ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
