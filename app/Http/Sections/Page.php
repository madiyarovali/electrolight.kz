<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;


/**
 * Class Page
 *
 * @property \App\Page $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Page extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
     public function initialize()
     {
        $this->addToNavigation($priority = 500, function() {
            return \App\Page::count();
        });
     }
     public function getTitle() {
        return 'Страницы';
     }
     public function getIcon()
     {
        return 'fa fa-file';
     }
    public function onDisplay()
    {
        return AdminDisplay::table()
                 ->setHtmlAttribute('class', 'table-primary')
                 ->setColumns(
                     AdminColumn::text('id', '#')->setWidth('30px'),
                     AdminColumn::link('title', 'Заголовок')->setWidth('100px')
                 )
                 ->setApply(function ($query) {
                     $query->orderBy('created_at', 'desc');
                 })
                 ->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Заголовок')->required(),
            AdminFormElement::text('path', 'Ссылка')->required(),
            AdminFormElement::wysiwyg('body', 'Текст')->required()
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
