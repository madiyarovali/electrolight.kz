<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

/**
 * Class Order
 *
 * @property \App\Order $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Order extends Section implements Initializable
{

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
     public function initialize()
     {
        $this->addToNavigation($priority = 500, function() {
            return \App\Order::count();
        });
     }
     public function getTitle() {
        return 'Заказы';
     }
     public function getIcon()
     {
        return 'fa fa-inbox';
     }
    public function onDisplay()
    {
        return AdminDisplay::table()->with('user')
                 ->setHtmlAttribute('class', 'table-primary')
                 ->setColumns(
                   AdminColumn::text('id', 'Номер заказа')->setWidth('30px'),
                   AdminColumn::text('status', 'Cтатус')->setWidth('100px'),
                   AdminColumn::link('item.title', 'Товар' )->setWidth('100px'),
                   AdminColumn::text('user.name', 'Покупатель' )->setWidth('100px'),
                   AdminColumn::datetime('created_at', 'Дата' )->setWidth('100px')
                 )
                 ->setApply(function ($query) {
                     $query->orderBy('created_at', 'desc');
                     $query->where('status', '<>',0);
                 })
                 ->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('id', 'Номер заказа')->required(),
            AdminFormElement::select('status', 'Статус')->setOptions([0 => 'В корзине', 1 => 'В рассмотрении', 2 => 'Заказ принят', 3 => 'Отменен', 4 => 'Отказано', 5 => 'Успешно выслан'])
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
