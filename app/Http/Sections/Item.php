<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

/**
 * Class Item
 *
 * @property \App\Item $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Item extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
     public function initialize()
     {
        $this->addToNavigation($priority = 500, function() {
            return \App\Item::count();
        });
     }
     public function getTitle() {
        return 'Продукты';
     }
     public function getIcon()
     {
        return 'fa fa-square';
     }
    public function onDisplay()
    {
        return AdminDisplay::table()
                 ->setHtmlAttribute('class', 'table-primary')
                 ->setColumns(
                     AdminColumn::text('id', '#')->setWidth('30px'),
                     AdminColumn::link('title', 'Наименование')->setWidth('100px')
                 )
                 ->setApply(function ($query) {
                     $query->orderBy('id', 'ASC');
                 })
                 ->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Наименование')->required(),
            AdminFormElement::wysiwyg('body', 'Описание')->required(),
            AdminFormElement::number('price', 'Цена')->required(),
            AdminFormElement::select('is_able', 'В наличии?')->setOptions([1 => 'В наличии', 0 => 'Под заказ'])->required(),
            AdminFormElement::number('is_spec', 'Скидки в %'),
            AdminFormElement::select('category_id', 'Категория', \App\Category::whereNotIn('parent', [0,1])->pluck('title', 'id')->all())->required(),
            AdminFormElement::image('image', "Главная фотография")->setUploadPath(function(\Illuminate\Http\UploadedFile $file) {
                return 'images/uploads';
            }),
            AdminFormElement::text('code_item', 'Код товара')->required(),
            AdminFormElement::text('guarant', 'Гарантия'),
            AdminFormElement::text('artic', 'Артикул'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
