<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user() == null){
            return redirect('/');
        }
        else if (Auth::user()->email != "ali.ongarbekovich@gmail.com") {
            return redirect('/');
        }
        return $next($request);
    }
}
