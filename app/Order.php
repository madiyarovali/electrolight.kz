<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'qty',
        'status',
        'item_id'
    ];

    public static function cart()
    {
        $orders = Order::where('user_id', Auth::id())->whereIn('status', [0])->get();
        return $orders;
    }

    // belongsTo
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function item()
    {
        return $this->belongsTo('App\Item');
    }
}
