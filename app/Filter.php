<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable = [
        'parameterName',
        'parameters',
        'category_id'
      ];

      public function category()
      {
          return $this->belongsTo('App\Category');
      }

      public static function getParameterName($category_id)
      {
          $parameterName = Filter::where('category_id', $category_id)->get();
          return $parameterName;
      }

      public static function getParameters($id)
      {
        $parameterName = Filter::where('id', $id)->value('parameters');

        $array = explode(', ', $parameterName);

        // remove all empty values from array
        $array = array_filter($array, function($value) { return $value !== ''; });
        // ----------------------------------
        return $array;
      } 
}
